FROM python:3.9-alpine3.14
LABEL maintainer="Cody Neiman <cody-neiman@cody.to>"

COPY . .

RUN apk update && apk upgrade -U -a

RUN pip install --no-cache-dir --upgrade pip && pip install --no-cache-dir -r ./requirements.txt

ENTRYPOINT ["./docker-run.sh"]
