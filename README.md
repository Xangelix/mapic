# mapic

## Usage

1. Install halcyon cli locally: `python3 -m pip install halcyon`
2. Generate a token for logging in your bot: `python3 -m halcyon -s <HOMESERVER_URL> -u @<BOT_USERNAME>:<HOMESERVER_URL> -p <BOT_PASSWORD>`
3. Create a secret using that token: `echo "<TOKEN>" | sudo docker secret create tkn -`
4. Build your container: `sudo docker build -t "<DOCKER_HUB_USERNAME>/<BOT_NAME>:<VERSION>" .`
5. Update docker-compose.yml: Change `xangelix/mapic:latest` to `<DOCKER_HUB_USERNAME>/<BOT_NAME>:<VERSION>`
6. Create your stack, it will automatically run: `sudo docker stack deploy --compose-file=docker-compose.yml <STACK_NAME>`
7. To make changes, stop and remove your stack, rebuild your container, and deploy again: `sudo docker stack rm <STACK_NAME> && sudo docker build -t "<DOCKER_HUB_USERNAME>/<BOT_NAME>:<VERSION> . && sudo docker stack deploy --compose-file=docker-compose.yml <STACK_NAME>`
