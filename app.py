import requests
import json

import halcyon


def get_secret(secret_name):
    try:
        with open("/run/secrets/{0}".format(secret_name), "r") as secret_file:
            return secret_file.read()
    except IOError:
        return None


client = halcyon.Client()


@client.event
async def on_room_invite(room):
    """On room invite, autojoin and report join"""
    print("Invited to join", room.name)
    await client.join_room(room.id)
    await client.send_message(room.id, body="Joined")


@client.event
async def on_message(message):
    """If we see a message with the phrase 'give me random', do a reply message with 32 random characters"""
    print(message.event.id)
    if "give me random" in message.content.body:
        await client.send_typing(message.room.id)
        body = f"This looks random: {requests.get('https://random.wesring.com').json()['value']}"
        await client.send_message(message.room.id, body=body, replyTo=message.event.id)


@client.event
async def on_ready():
    print("Online!")
    await client.change_presence(statusMessage="indexing /dev/urandom")


if __name__ == "__main__":
    client.run(halcyonToken=get_secret("tkn"))
